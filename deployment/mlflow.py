# -*- coding: utf-8 -*-
"""MLflow.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1rU-jqepcE-kp9UWPs_xbYKzo457tfSoF
"""

!pip install mlflow

!pip install pyngrok

!pip install streamlit



# Commented out IPython magic to ensure Python compatibility.
# %%writefile app.py
# import mlflow
# import streamlit as st
# import warnings 
# warnings.filterwarnings("ignore")
# import numpy as np
# import pandas as pd
# from sklearn.tree import DecisionTreeRegressor
# from sklearn.preprocessing import LabelEncoder, StandardScaler
# from sklearn.metrics import mean_squared_error
# 
# # Define Streamlit UI components
# st.title("Categorical Prediction")
# train_data = st.file_uploader("Upload a train file")
# test_data = st.file_uploader("Upload a test file")
# 
# # Log file on upload
# if train_data is not None:
#     train_df = pd.read_csv(train_data)
#     if test_data is not None:
#         test_df = pd.read_csv(test_data)
# 
#         # Encode categorical features
#         encoder = LabelEncoder()
#         for col in train_df.select_dtypes(include=['object']):
#             train_df[col] = encoder.fit_transform(train_df[col])
#             test_df[col] = encoder.transform(test_df[col])
# 
#         # Prepare data for modeling
#         X_train = train_df.drop(['id', 'target'], axis=1)
#         y_train = train_df['target']
#         X_test = test_df.drop('id', axis=1)
# 
#         scale = StandardScaler().fit(X_train)
#         X_train_scaled = scale.transform(X_train)
# 
#         # Fit Decision Tree Regressor model
#         with mlflow.start_run():
#             model = DecisionTreeRegressor(random_state=0)
#             model.fit(X_train_scaled, y_train)
# 
#             # Log model parameters
#             mlflow.log_param("max_depth", model.get_params()['max_depth'])
#             mlflow.log_param("min_samples_split", model.get_params()['min_samples_split'])
#             mlflow.log_param("min_samples_leaf", model.get_params()['min_samples_leaf'])
#             mlflow.log_param("criterion", model.get_params()['criterion'])
#             mlflow.log_param("splitter", model.get_params()['splitter'])
#             mlflow.log_param("model", "DecisionTreeRegressor")
# 
#             X_test_scaled = scale.transform(X_test)
#             X_test_arr = np.array(X_test_scaled)
# 
#             # Predict target for test set
#             y_test_pred = model.predict(X_test_arr)
# 
#             # Calculate RMSE
#             y_train_pred = model.predict(X_train_scaled)
#             train_rmse = mean_squared_error(y_train, y_train_pred, squared=False)
#             mlflow.log_metric("train_rmse", train_rmse)
# 
#             submission = test_df[['id']].copy()
#             submission['target'] = y_test_pred
#             csv_file = submission.to_csv(index=False)
# 
#             st.download_button('Download file', data=csv_file, file_name='submission.csv', mime='text/csv')
#

from pyngrok import ngrok

ngrok.set_auth_token("2NmpozMseEJY59TXYP8fnAQABm3_6xQru6WAQwTcQJFcRa8G3")

!nohup streamlit run app.py --server.port 80 &
url = ngrok.connect(port='80')
print(url)